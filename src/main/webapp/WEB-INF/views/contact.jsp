<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create Contact</title>

<link rel="icon"
	href="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
	type="image/x-icon">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />

<style type="text/css">
/* Split the screen in half */
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}

/* Control the left side */
.left {
  left: 0;
}
/* If you want the content centered horizontally and vertically */
.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

</style>
</head>
<body>

<div align="right">
        <a href="home" class="btn btn-info">Home</a> 
		<img src="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
			height="50" width="50">
</div>

<div class="container">
<div class="card">
<div class="card-header bg-primary text-white text-center" ><b>Contact Info</b></div>
<div align="center" class="card-body">
<form:form action="save" method="post" modelAttribute="contact">
<pre>
<b>Name</b>  : <form:input path="contactName"/><br/>
<b>Email</b> : <form:input path="contactEmail"/><br/>
<b>Phno</b>  : <form:input path="contactNumber"/><br/>
<input type="submit" value="Submit" class="btn btn-success"/>

<a href="viewall"><b><i>View All Contacts</i></b></a> 
</pre>
</form:form>
</div>

<div class="card-footer bg-white">
		<c:if test="${message!=null}">
			<div align="center" class="alert alert-info">${message}</div>
		</c:if>
</div>
</div>
</div>

<div class="split left">

<div class="centered">
		<img
			src="https://wpuploads.appadvice.com/wp-content/uploads/2013/01/contac.png"
			height="500" width="500">
</div>
</div>

</body>
</html>