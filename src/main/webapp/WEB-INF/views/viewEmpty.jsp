<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contacts</title>
<link rel="icon"
	href="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
	type="image/x-icon">
	
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />	
</head>
<body>
<div align="right">

		<a href="home" class="btn btn-info">Home</a> 
		<a href="contactinfo" class="btn btn-info">Create Contact</a> 
		<img src="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
			height="50" width="50">
</div>

<div class="card-footer bg-white" style="height: 50px,">
				<c:if test="${message!=null}">
					<div align="center" class="alert alert-info">${message}</div>
				</c:if>
</div>

<div align="center" class="alert alert-info">Please Add Contacts To View the List</div>

	<div align="left">
		<img
			src="https://wpuploads.appadvice.com/wp-content/uploads/2013/01/contac.png"
			height="420" width="420">
	</div>

</body>
</html>