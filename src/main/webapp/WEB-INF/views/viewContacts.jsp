<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contacts</title>

<link rel="icon"
	href="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
	type="image/x-icon">
	
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />		
	
</head>
<body>
<div align="right">
        <a href="home" class="btn btn-info">Home</a> 
		<img src="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
			height="50" width="50">
</div>

<div class="container">
		<div class="card">
			<div class="card-header bg-primary text-white text-center">
				<h3><b>All Contacts</b></h3>
			</div>
			<a href="contactinfo"><b><i>Add New Contact</i></b></a> 
			<div class="card-body">
			<table class="table table-hover" border="1">
					<tr class="bg-info text-white">
						<th>S.NO</th>
						<th>NAME</th>
						<th>EMAIL</th>
						<th>PHNO</th>
						<th>ACTION</th>
			</tr>
			
			<c:forEach items="${list}" var="ob">
			
			<tr>
				<td>${ob.contactId}</td>
				<td>${ob.contactName}</td>
				<td>${ob.contactEmail}</td>
				<td>${ob.contactNumber}</td>
				<td>
				    <a href="edit?id=${ob.contactId}" class="btn btn-warning">EDIT</a> 
					<a href="delete?id=${ob.contactId}"  class="btn btn-danger" onclick="return confirm('Do you want to Delete?')">DELETE</a>
				</td>
			</tr>
			</c:forEach>
			</table>
			</div>
			<div class="card-footer bg-white">
				<c:if test="${message!=null}">
					<div align="center" class="alert alert-info">${message}</div>
				</c:if>
			</div>
		</div>
</div>

<hr/>
		<!-- pagination starts -->
		<nav aria-label="Page navigation ">
		<div align="center">
		<c:if test="${!page.isFirst()}">
			<a href="?page=0" class="btn btn-warning">FIRST</a>  
		</c:if>
		
		<c:if test="${page.hasPrevious()}">
			<a href="?page=${page.number-1}" class="btn btn-warning">Previous</a>  
		</c:if>
		
		<c:forEach begin="0" end="${page.totalPages-1}" var="i">
			<a href="?page=${i}" class="btn btn-warning">${i+1}</a> &nbsp;&nbsp;
		</c:forEach>
		
		<c:if test="${page.hasNext()}">
			<a href="?page=${page.number+1}" class="btn btn-warning">Next</a>  
		</c:if>
		
		<c:if test="${!page.isLast()}">
			<a href="?page=${page.totalPages-1}" class="btn btn-warning">LAST</a>  
		</c:if>
	</div>
		</nav>
		<!-- pagination ends -->
	<hr/>
	
</body>
</html>