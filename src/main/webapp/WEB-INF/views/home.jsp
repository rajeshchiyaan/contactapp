<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Contacts</title>
<!-- add icon link -->
<link rel="icon"
	href="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
	type="image/x-icon">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />


<style>
h1 {
	color: navy;
	margin-left: 20px;
}

.textbox {
	background: white;
	border: 1px solid #DDD;
	border-radius: 5px;
	box-shadow: 0 0 5px #DDD inset;
	color: #666;
	outline: none;
	height: 32px;
	width: 200px;
}

/* body
{
    background-image: url('https://ak5.picdn.net/shutterstock/videos/4572215/thumb/1.jpg');
} */
</style>

<script>
      function getSearch() {
        // Selecting the input element and get its value 
        let inputVal = document.getElementById("search").value;
      }
    </script>


</head>
<body>

	<div align="right">

		<input type="text" class="textbox" id="search" placeholder="search..." />
		<a href="" class="btn btn-info" onclick="getSearch()">Search</a> 
		<a href="viewall" class="btn btn-info">View All</a> 
		<a href="contactinfo" class="btn btn-info">Create Contact</a> 
		<img src="https://cdn0.iconfinder.com/data/icons/simpline-mix/64/simpline_49-512.png"
			height="50" width="50">
			

	</div>
	
	<div align="left">
		<h1><b><i>Contacts</i></b></h1>
	</div>
	
	<div align="left">
		<img
			src="https://wpuploads.appadvice.com/wp-content/uploads/2013/01/contac.png"
			height="500" width="500">
	</div>

</body>
</html>