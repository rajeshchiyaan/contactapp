package com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMvcContactAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcContactAppApplication.class, args);
	}

}
