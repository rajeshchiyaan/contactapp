package com.example.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.app.entity.Contact;
import com.example.app.repo.ContactRepository;
import com.example.app.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ContactRepository contactRepo;

	@Override
	public boolean saveContact(Contact contact) {

		Integer contactId = contactRepo.save(contact).getContactId();

		return contactId > 0;
	}

	@Override
	public Page<Contact> getAllContacts(Pageable pageable) {
//		return (Page<Contact>) contactRepo.findAll(pageable).stream().filter(element -> element.getActiveSwitch().equals('Y'))
//				.collect(Collectors.toList());
		
		return contactRepo.findAll(pageable);
	}

	@Override
	public Contact getContactById(Integer contactId) {
		return contactRepo.getContactById(contactId);
	}

	@Override
	public boolean deleteContactById(Integer contactId) {
		boolean existsById = contactRepo.existsById(contactId);
		if (existsById) {
			contactRepo.deleteById(contactId);
			return true;
		}
		return false;
	}

	@Override
	public boolean partialDeleteContactById(Integer contactId) {
		boolean existsById = contactRepo.existsById(contactId);
		if (existsById) {
			contactRepo.deleteContactById(contactId);
			return true;
		}
		return false;
	}

}
