package com.example.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.app.entity.Contact;

public interface ContactService {

	boolean saveContact(Contact contact);

	Page<Contact> getAllContacts(Pageable pageable);

	Contact getContactById(Integer contactId);

	boolean deleteContactById(Integer contactId);
	
	boolean partialDeleteContactById(Integer contactId);

}
