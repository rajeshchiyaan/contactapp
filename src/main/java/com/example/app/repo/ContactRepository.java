package com.example.app.repo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.app.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Serializable> {

	@Query(value = "SELECT CONTACT_ID,CONTACT_NAME,CONTACT_EMAIL,CONTACT_NUMBER FROM CONTACT_DETAILS WHERE ACTIVE_SW <> 'N'", nativeQuery = true)
	List<Contact> getAllContacts();

	@Query(value = "SELECT * FROM CONTACT_DETAILS C WHERE C.CONTACT_ID= ?1", nativeQuery = true)
	Contact getContactById(Integer contactId);

	@Query(value = "UPDATE CONTACT_DETAILS SET ACTIVE_SW='N',UPDATED_DATE=SYSDATE WHERE CONTACT_ID=?1", nativeQuery = true)
	void deleteContactById(Integer contactId);

}
