package com.example.app.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.app.entity.Contact;
import com.example.app.service.ContactService;

@Controller
public class ContactController {
	
	@Autowired
	private ContactService contactService;
	
	@GetMapping("/contactinfo")
	public String showForm(Model model)
	{
		
		model.addAttribute("contact", new Contact());
		return "contact";
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute Contact contact,RedirectAttributes model)
	{

		contact.setCreatedDate(LocalDate.now());
		contact.setActiveSwitch('Y');
		
		boolean localIsContact = contactService.saveContact(contact);

		if (localIsContact) {
			model.addFlashAttribute("message", "Contact Saved Successfully");
		}
		else
		{
			model.addFlashAttribute("message", "Contact Not Saved Successfully");
		}

		return "redirect:/contactinfo";
	}
	
	@GetMapping("/viewall")
	public String viewAllContacts(@PageableDefault(page=0,size=1) Pageable pageable,Model model)
	{
		
		Page<Contact> page = contactService.getAllContacts(pageable);
		
		model.addAttribute("list", page.getContent());
		model.addAttribute("page", page);
		
		if(page.getContent().size()>0)
		{
			return "viewContacts";
		}
		else
		{
			return "viewEmpty";
		}
		
	}
	
	@GetMapping("/edit")
	public String updateContact(@RequestParam("id") Integer id,Model model)
	{
		Contact contact = contactService.getContactById(id);
		
		model.addAttribute("contact", contact);
		
		return "contactEdit";
	}
	
	@PostMapping("/update")
	public String update(@ModelAttribute Contact contact,Model model)
	{

		contact.setUpdatedDate(LocalDate.now());
		boolean localIsContact = contactService.saveContact(contact);

		if (localIsContact) {
			model.addAttribute("message", "Contact Updated Successfully");
		}
		else
		{
			model.addAttribute("message", "Contact Not Updated Successfully");
		}

		return "contactEdit";
	}
	
	@GetMapping("/delete")
	public String deleteContact(
			@RequestParam("id")Integer id,RedirectAttributes model)
	{
		// call service layer for delete
		boolean deleteContactById = contactService.deleteContactById(id);

		if (deleteContactById) {
			model.addFlashAttribute("message", "Contact Deleted Successfully");
		}
		else
		{
			model.addFlashAttribute("message", "Contact Not Deleted Successfully");
		}

		// return ViewName
		return "redirect:/viewall";

	}

}
